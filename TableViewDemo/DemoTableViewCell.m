//
//  DemoTableViewCell.m
//  TableViewDemo
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "DemoTableViewCell.h"

@interface DemoTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *theLabel;
@end


@implementation DemoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.contentView.backgroundColor = UIColor.whiteColor;
}

// In reality, instead of id it would be whatever data object your app is using (e.g. Pokemon)
- (void)configureCell:(id)theObject
{
    _dataObject = theObject;
    NSString *dataObject = theObject;
    self.theLabel.text = dataObject;
    /*
    if (indexPath.row % 2 == 0) {
        self.contentView.backgroundColor = UIColor.greenColor;
    }
     */
}

@end
