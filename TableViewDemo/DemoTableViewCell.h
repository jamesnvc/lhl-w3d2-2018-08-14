//
//  DemoTableViewCell.h
//  TableViewDemo
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoTableViewCell : UITableViewCell

@property (readonly, strong, nonatomic) id dataObject;

- (void)configureCell:(id)theObject;

@end
