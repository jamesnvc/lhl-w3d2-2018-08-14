//
//  DetailViewController.h
//  TableViewDemo
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
// outlets should be private (i.e. in the .m file for reasons explained in prepareForSegue
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

// in general, this would be our more complicated data object (e.g. a Pokemon object)
@property (strong,nonatomic) NSString *detailObject;

@end
