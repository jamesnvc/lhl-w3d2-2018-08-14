//
//  ViewController.m
//  TableViewDemo
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "DemoTableViewCell.h"
#import "DetailViewController.h"

@interface ViewController ()

/* Not optimal way
@property (nonatomic,strong) NSArray *goodPokemon;
@property (nonatomic,strong) NSArray *notGoodPokemon;
*/

@property (nonatomic,strong) NSArray<NSArray*> *pokemon;
@property (nonatomic,strong) NSArray<NSString*>* sectionTitles;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    /*
    self.goodPokemon = @[@"Pikachu", @"Bulbasaur", @"Charmander",
                     @"Ghastly"];
    self.notGoodPokemon = @[@"Metapod", @"Weedle", @"Magikarp"];
     */
    self.pokemon = @[
                     @[@"Pikachu", @"Bulbasaur", @"Charmander",                     @"Ghastly"],
                     @[@"Mr. Mime", @"Jynx", @"Mewtoo"],
                     @[@"Metapod", @"Weedle", @"Magikarp", @"Voltorb"]
                     ];

    self.sectionTitles = @[@"Good Pokemon",
                           @"Creepy Pokemon",
                           @"Less-than-good pokemon"];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"pokemonSelectedSegue"]) {
        DetailViewController *dvc = segue.destinationViewController;
        DemoTableViewCell *cell = sender;
        NSLog(@"Clicking on cell %@ going to %@", cell.dataObject, dvc);
        // THIS IS WRONG
        // because at this point, we're still showing this current view controller, so the view for the detail controller hasn't yet been loaded, so the nameLabel is nil!
//        dvc.nameLabel.text = cell.dataObject;
        // THIS IS THE PREFERRED WAY
        dvc.detailObject = cell.dataObject;
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return 2;
    return self.pokemon.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*
    if (section == 0)  {
        return self.goodPokemon.count;
    } else if (section == 1) {
        return self.notGoodPokemon.count;
    }
    return 0;
     */
    /*
    NSArray *sectionArray = self.pokemon[section];
    return sectionArray.count;
     */
    return self.pokemon[section].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DemoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"demoCell" forIndexPath:indexPath];

    // in reality, this would probably be some data object (e.g. a Pokemon object)
    NSString *pokemon = self.pokemon[indexPath.section][indexPath.row];
    [cell configureCell:pokemon];

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.sectionTitles[section];
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"selected", self.pokemon[indexPath.row]);
    DemoTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"Selected %@", cell.dataObject);
}

@end
